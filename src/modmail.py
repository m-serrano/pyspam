import imap_tools
from imap_tools import AND
import sys
import email

import os
imap_server = os.environ['IMAP_SERVER']
imap_user = os.environ['IMAP_USER']
imap_pwd =  os.environ['IMAP_PWD']

# conecta al servidor
server = imap_tools.MailBox(imap_server)
server.login(imap_user, imap_pwd, initial_folder='INBOX.INBOX.Junk')

# selecciona la carpeta de entrada
#server.folder_select('INBOX')

# busca el correo específico que quieres cambiar
#message = server.search('SUBJECT "El asunto original"')[0]
messages = server.fetch(AND(subject='[SPAM] '+sys.argv[1]))

for msg in messages:
    print(msg.uid,"*", msg.date, msg.subject, len(msg.text or msg.html))
    #msg.subject= msg.subject.replace("[SPAM]", "").strip()
#    msg.subject="Factura"
    new_mail = email.message_from_bytes(msg.obj.as_bytes())

    # cambia el asunto del nuevo correo
    new_mail.replace_header("Subject", new_mail["Subject"].replace("[SPAM]", "").strip())
    server.folder.set('INBOX')
    server.append(new_mail.as_bytes())

# borra el correo original
#server.message_delete(message)

# cierra la conexión al servidor
server.logout()
