import imap_tools
from imap_tools import AND

import email
from email import encoders
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from datetime import datetime

import os
imap_server = os.environ['IMAP_SERVER']
imap_user = os.environ['IMAP_USER']
imap_pwd =  os.environ['IMAP_PWD']
webmail_server =  os.environ['WEBMAIL_SERVER']
imap_admin = os.environ['IMAP_ADMIN']
MAX_CHECK = int(os.environ['MAX_CHECK'])  #288=24 horas=1440 minutos... En bloques de 5 minutos=288

with open('tempo_n_check', 'r') as f:
    contenido = f.read()
    n_check = int(contenido)
    #print(n_check)
    
with open('tempo_n_mails', 'r') as f:
    contenido = f.read()
    n_mails = int(contenido)
    #print(n_check)

# conecta al servidor IMAP
server = imap_tools.MailBox(imap_server)
server.login(imap_user, imap_pwd, initial_folder='INBOX')

#Listar folders
#for f in server.folder.list('INBOX'):
#    print(f)  # FolderInfo(name='INBOX|cats', delim='|', flags=('\\Unmarked', '\\HasChildren'))

# busca todos los correos marcados con "[SPAM]" en el asunto

#Mover todos los mensajes da error con fetch.. habría que limtiarlo a 1. Está explicado en la documentación de imap_tools
#messages = server.fetch(AND(subject='[SPAM]'),limit=1)
messages = server.fetch(AND(subject='[SPAM]'))
#construye body
#body = '<ul>'
#i=0

for msg in messages:
    #print(msg.uid,"*", msg.date, msg.subject, len(msg.text or msg.html))
    #body+=f'<li>{msg.date} - <a href="{webmail_server}?_task=mail&_mbox=INBOX.INBOX.Junk&_uid={msg.uid}&_action=show">{msg.subject}</a></li>'
    #i+=1
    body=f'<li>{msg.date} - <a href="{webmail_server}?_task=mail&_mbox=INBOX.INBOX.Junk&_uid={msg.uid}&_action=show">{msg.subject}</a></li>\n'
    with open('tempo_mail', 'a') as f:
#     # Añadimos el texto al archivo
        f.write(body)
    n_mails+=1
    print(body)
    #server.move(msg.uid, 'INBOX.INBOX.Junk')

messages=server.uids(AND(subject='[SPAM]'))
# mueve cada correo encontrado a la carpeta SPAM. Hay que hacerlo con la búsqueda de uids
for msg in messages:
    server.move(msg, 'INBOX.INBOX.Junk')

if n_check<MAX_CHECK:
    n_check+=1
else:
    n_check=0
    if n_mails>0:
        # Obtén la fecha actual
        now = datetime.now()
        # Formatea la fecha como desees
        date_string = now.strftime("%d-%m-%Y")
        introduccion = f"Hola, has recibido {n_mails} correos sospechosos. Están todos en la carpeta de SPAM. Revísalos y si hay algún error, <a href='mailto:{imap_admin}'>mándale un correo al administrador</a>"
        body=introduccion+'<ul>'
        with open('tempo_mail', 'r') as f:
    # Leemos el contenido del archivo
            contenido = f.read()
            body=body+contenido+'</ul>'
        
        print(body)
        n_mails=0
        with open('tempo_mail', 'w') as f:
            f.write('')
        
        correo = MIMEMultipart()
        correo['From'] = imap_admin
        correo['To'] = imap_user
        correo['Subject'] = f'Correos sospechosos {date_string}'
        correo.attach(MIMEText(body, 'html'))
    
        # Escribir el mensaje en un archivo
        with open('email.eml', 'w') as f:
            f.write(correo.as_string())
    
        # APPEND: add message to mailbox directly, to INBOX folder with \Seen flag and now date
        with open('email.eml', 'rb') as f:
            msg = imap_tools.MailMessage.from_bytes(f.read())  # *or use bytes instead MailMessage
        server.append(msg, 'INBOX', dt=None, flag_set=[imap_tools.MailMessageFlags.SEEN])

#generar correo si hay mensajes
# if i>0:
#     # Obtén la fecha actual
#     now = datetime.now()
#     # Formatea la fecha como desees
#     date_string = now.strftime("%d-%m-%Y")
#     introduccion = f"Hola, hoy has recibido {i} correos sospechosos. Están todos en la carpeta de SPAM. Revísalos y si hay algún error, <a href='mailto:{imap_admin}'>mándale un correo al administrador</a>"
#     body=introduccion+body
#     print(body)
#     correo = MIMEMultipart()
#     correo['From'] = imap_admin
#     correo['To'] = imap_user
#     correo['Subject'] = f'Correos sospechosos {date_string}'
#     correo.attach(MIMEText(body, 'html'))
#
#     # Escribir el mensaje en un archivo
#     with open('email.eml', 'w') as f:
#         f.write(correo.as_string())
#
#     # APPEND: add message to mailbox directly, to INBOX folder with \Seen flag and now date
#     with open('email.eml', 'rb') as f:
#         msg = imap_tools.MailMessage.from_bytes(f.read())  # *or use bytes instead MailMessage
#     server.append(msg, 'INBOX', dt=None, flag_set=[imap_tools.MailMessageFlags.SEEN])

# cierra la conexión al servidor
with open('tempo_n_check', 'w') as f:
    f.write(str(n_check))
with open('tempo_n_mails', 'w') as f:
    f.write(str(n_mails))

server.logout()
